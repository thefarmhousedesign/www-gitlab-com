---
layout: markdown_page
title: "DevSecOps Adoption"
description: "A Working Group focused on the Top 12 Cross-Functional initiative of ensuring ultimate customers successfully adopt devsecops within 2 months of purchase
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value          |
|-----------------|----------------|
| Date Created    | October 19 2022   |
| End Date        | TBD |
| Slack           | #wg_devsecops_adoption (only accessible from within the company) |
| Epic            | [DevSecOps Adoption Working Group](https://gitlab.com/groups/gitlab-com/sales-team/-/epics/59) (only accessible from within the company)
| Google Doc      | [DevSecOps Adoption WG Agenda](https://docs.google.com/document/d/1kUrBpNYhHZNpWvihQe2484FAAy5sO42yVKR7uINULh0/edit) (only accessible from within the company) |

## Business Goal

No customers in red on DevSecOps Adoption within 3 months of purchase.

## Exit Criteria
Run articulated process with 10 customers with successful adoption within 3 months
* 


## Roles and Responsibilities

| Working Group Role    | Person                | Title                                  | 
|-----------------------|-----------------------|----------------------------------------|
| Executive Stakeholder | Michael McBride       | CRO
| DRI                   | Sherrod Patching      | VP, Customer Success Management        |
| Member                | Francis Ofungwu       | Field CISO                        |
| Member                | Jonathan Fullam       | Sr Director, Solution Architecture           |
| Member                | Melani  Ross          | Director, Professional Services |
| Member                | Tanya Helin           | RVP, Enterprise East                   |
| Member                | Rich Phillips         | ASM, Enterprise East         |
| Member                | Nick Christou         | Director of Sales, SMB                         |
| Member                | Hillary Benson        | Director, Product Management            |
| Member                | Nick Wilson           | Sr Product Manager - Govern  |
| Member                | Derek Ferguson        | Sr Product Manager - Secure and Govern | 
| Member                | Saumya Upadhyaya      | Principal Product Marketing Manager |
| Member                | Jeff Beaumont         | Sr. Director CS Operations |

